package com.example.gonzalodelatorre;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adaptador extends ArrayAdapter<Ciudad> {

    Activity contexto;

    public Adaptador(Activity contexto, ArrayList<Ciudad> datos) {
        super(contexto, R.layout.elemento_lista, datos);
        this.contexto = contexto;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = contexto.getLayoutInflater();
        View item = inflater.inflate(R.layout.elemento_lista, null);

        Ciudad mielemento=getItem(position);

        TextView titulo = (TextView) item.findViewById(R.id.textView_Titulo);
        TextView ciudad = (TextView) item.findViewById(R.id.textView_Ciudad);

        ciudad.setText(mielemento.getName());

        return(item);
    }

}
