package com.example.gonzalodelatorre;

public class Pais {

    String Code;
    String Name;
    String Continent;
    String Region;
    int SurfaceArea;
    int IndepYear;
    int Population;
    int LifeExpectancy;
    int GNP;
    int GNPOld;
    String LocalName;
    String GovernmentForm;
    String HeadOfState;
    int Capital;
    String Code2;
    String tabla;

    public Pais(String code, String name, String continent, String region, int surfaceArea, int indepYear, int population, int lifeExpectancy, int GNP, int GNPOld, String localName, String governmentForm, String headOfState, int capital, String code2, String tabla) {
        Code = code;
        Name = name;
        Continent = continent;
        Region = region;
        SurfaceArea = surfaceArea;
        IndepYear = indepYear;
        Population = population;
        LifeExpectancy = lifeExpectancy;
        this.GNP = GNP;
        this.GNPOld = GNPOld;
        LocalName = localName;
        GovernmentForm = governmentForm;
        HeadOfState = headOfState;
        Capital = capital;
        Code2 = code2;
        this.tabla = tabla;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getContinent() {
        return Continent;
    }

    public void setContinent(String continent) {
        Continent = continent;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String region) {
        Region = region;
    }

    public int getSurfaceArea() {
        return SurfaceArea;
    }

    public void setSurfaceArea(int surfaceArea) {
        SurfaceArea = surfaceArea;
    }

    public int getIndepYear() {
        return IndepYear;
    }

    public void setIndepYear(int indepYear) {
        IndepYear = indepYear;
    }

    public int getPopulation() {
        return Population;
    }

    public void setPopulation(int population) {
        Population = population;
    }

    public int getLifeExpectancy() {
        return LifeExpectancy;
    }

    public void setLifeExpectancy(int lifeExpectancy) {
        LifeExpectancy = lifeExpectancy;
    }

    public int getGNP() {
        return GNP;
    }

    public void setGNP(int GNP) {
        this.GNP = GNP;
    }

    public int getGNPOld() {
        return GNPOld;
    }

    public void setGNPOld(int GNPOld) {
        this.GNPOld = GNPOld;
    }

    public String getLocalName() {
        return LocalName;
    }

    public void setLocalName(String localName) {
        LocalName = localName;
    }

    public String getGovernmentForm() {
        return GovernmentForm;
    }

    public void setGovernmentForm(String governmentForm) {
        GovernmentForm = governmentForm;
    }

    public String getHeadOfState() {
        return HeadOfState;
    }

    public void setHeadOfState(String headOfState) {
        HeadOfState = headOfState;
    }

    public int getCapital() {
        return Capital;
    }

    public void setCapital(int capital) {
        Capital = capital;
    }

    public String getCode2() {
        return Code2;
    }

    public void setCode2(String code2) {
        Code2 = code2;
    }

    public String getTabla() {
        return tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }
}
