package com.example.gonzalodelatorre;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class PaisBDAdapter {

    public static final String TABLA_BD = "country";

    public static final String CAMPO_CODE = "Code";
    public static final String CAMPO_NAME = "Name";
    public static final String CAMPO_CONTINENT = "Continent";
    public static final String CAMPO_REGION = "Region";
    public static final String CAMPO_SURFACEAREA = "SurfaceArea";

    public static final String CAMPO_INDEPYEAR = "IndepYearIndepYear";
    public static final String CAMPO_POPULATION = "Population";
    public static final String CAMPO_LIFEEXPECTANCY = "LifeExpectancy";
    public static final String CAMPO_GNP = "GNP";
    public static final String CAMPO_GNPOLD = "GNPOld";

    public static final String CAMPO_LOCALNAME = "LocalName";
    public static final String CAMPO_GOVERNMENTFORM = "GovernmentForm";
    public static final String CAMPO_HEADOFSTATE = "HeadOfState";
    public static final String CAMPO_CAPITAL = "Capital";
    public static final String CAMPO_CODE2 = "Code2";

    public static final String CREATE_TABLE = "create table "+TABLA_BD+" ("+CAMPO_CODE+" text, "+CAMPO_NAME+" text, "+CAMPO_CONTINENT+" text, "+CAMPO_REGION+" text, "+CAMPO_SURFACEAREA+ " integer, "
            +CAMPO_INDEPYEAR+" integer, "+CAMPO_POPULATION+" integer, "+CAMPO_LIFEEXPECTANCY+" integer, "+CAMPO_GNP+" integer, "
            +CAMPO_GNPOLD+" integer, "+CAMPO_LOCALNAME+" text, "+CAMPO_GOVERNMENTFORM+" text, "+CAMPO_HEADOFSTATE+" text, "+CAMPO_CAPITAL+" integer, "+CAMPO_CODE2+" text);";

    private Context contexto;
    private SQLiteDatabase bd;
    private CiudadBDHelper helper;

    public PaisBDAdapter(Context context) {
        this.contexto = context;
    }

    public PaisBDAdapter abrir() throws SQLException {
        helper = new CiudadBDHelper(this.contexto, "world.bd", null, 1);
        bd = helper.getWritableDatabase(); //Muy importante para abrir una bbdd, primero creas la tabla('helper') y despues hay que hacer esta línea
        return null;
    }

    public void cerrar(){
        helper.close();
    }


    // Método que crea un objeto ContentValues con los parámetros indicados
    private ContentValues generarContentValues(String Code, String Name, String Continent, String Region, String SurfaceArea,
                                               String IndepYear, String Population, String LifeExpectancy, String GNP, String GNPOld,
                                               String LocalName, String GovernmentForm, String HeadOfState, String Capital, String Code2){
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_CODE, Code);
        valores.put(CAMPO_NAME, Name);
        valores.put(CAMPO_CONTINENT, Continent);
        valores.put(CAMPO_REGION, Region);
        valores.put(CAMPO_SURFACEAREA, SurfaceArea);

        valores.put(CAMPO_INDEPYEAR, IndepYear);
        valores.put(CAMPO_POPULATION, Population);
        valores.put(CAMPO_LIFEEXPECTANCY, LifeExpectancy);
        valores.put(CAMPO_GNP, GNP);
        valores.put(CAMPO_GNPOLD, GNPOld);

        valores.put(CAMPO_LOCALNAME, LocalName);
        valores.put(CAMPO_GOVERNMENTFORM, GovernmentForm);
        valores.put(CAMPO_HEADOFSTATE, HeadOfState);
        valores.put(CAMPO_CAPITAL, Capital);
        valores.put(CAMPO_CODE2, Code2);

        return valores;
    }


    public Cursor buscar(String name){
        Cursor b = bd.rawQuery("select CC.code from city C join country CC where C.countrycode=CC.code and C.Name='"+name+"';",null);
        return b;
        //  String buscar = "select "+CAMPO_NAME+" from city where "+CAMPO_NAME+" = '"+name+"';";
      //  bd.execSQL(buscar);
    }

    //select CC.Code from city C join country CC where C.countrycode=CC.code;

    //Cargar el cursor para poder hacer un 'select*from nombreTabla;' y así recorrer la bbdd para cualquier utilidad necesaria;
    public Cursor cargarCursorPais(){
        Cursor b = bd.rawQuery("SELECT Code, Name, Continent, Region, SurfaceArea, IndepYear, Population, LifeExpectancy, GNP, GNPOld, LocalName, GovernmentForm, HeadOfState, Capital, Code2 FROM country;",null);
        return b;
    }

}
