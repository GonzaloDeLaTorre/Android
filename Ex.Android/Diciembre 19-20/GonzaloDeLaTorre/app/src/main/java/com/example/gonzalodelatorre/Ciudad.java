package com.example.gonzalodelatorre;

public class Ciudad {

    int id;
    String Name;
    String CountryCode;
    String District;
    int Population;
    String tabla;

    public Ciudad(int id, String name, String countryCode, String district, int population, String tabla) {
        this.id = id;
        Name = name;
        CountryCode = countryCode;
        District = district;
        Population = population;
        this.tabla = tabla;
    }

    public Ciudad(String name, String city) {
        Name = name;
        this.tabla = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }

    public int getPopulation() {
        return Population;
    }

    public void setPopulation(int population) {
        Population = population;
    }

    public String getTabla() {
        return tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }
}
