package com.example.gonzalodelatorre;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.database.Cursor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView titulo;
    Button buscar;

    TextView informacion1;
    TextView informacion2;
    TextView informacion3;
    TextView informacion4;

    private PaisBDAdapter baseDatos2;
    ArrayList<Pais> al2;

    private CiudadBDAdapter baseDatos;
    private Cursor cursor;
    private ListView lista;
    Adaptador adaptador;
    ArrayList<Ciudad> al;
    AdapterView.AdapterContextMenuInfo info; //Instanciar mediante AdapterView el adaptador del Menu contextual

    private FragmentManager fm = getSupportFragmentManager();   //Para sacar el Edit Dialog Fragment

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titulo = (TextView) findViewById(R.id.editText_Nombre);
        buscar = (Button) findViewById(R.id.button_Buscar);
        lista = (ListView) findViewById(R.id.MiLista);

        informacion1 = (TextView) findViewById(R.id.textView_Informacion);
        informacion2 = (TextView) findViewById(R.id.textView_Pais);
        informacion3 = (TextView) findViewById(R.id.textView_Distrito);
        informacion4 = (TextView) findViewById(R.id.textView_Poblacion);

        //Crear la BBDD
        baseDatos = new CiudadBDAdapter(this);
        //Abrir la BBDD (Se puede abrir directamente al crearla, pero yo he decidido abrirlo desde un metodo)
        baseDatos.abrir();

        //Crear la BBDD
        baseDatos2 = new PaisBDAdapter(this);
        //Abrir la BBDD (Se puede abrir directamente al crearla, pero yo he decidido abrirlo desde un metodo)
        baseDatos2.abrir();

        //Crear ArrayList de las Ciudad
        al = new ArrayList<Ciudad>();
        //Crear Adaptador
        adaptador = new Adaptador(this, al);
        //'Adaptar' la Lista al Adaptador con sus valores
        lista.setAdapter(adaptador);
        //Registrar el Menu contextual
        registerForContextMenu(lista);
        //Añadir elementos al ArrayList Ciudad
        añadirElementosAL();

        //Crear ArrayList de las Ciudad
        al2 = new ArrayList<Pais>();
        //Añadir elementos al ArrayList Pais
        añadirElementosAL2();

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Cargar cursor
                cursor = baseDatos.buscar(titulo.getText().toString());
                //Limpiar arraylist para rellenarlo con los datos que se van a recoger de la bbdd.
                al.clear();
                //Recoger los datos de la bbdd y añadirlos al arraylist
                for(int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    String Name = cursor.getString(0);

                    Ciudad temp = new Ciudad(Name, "city");
                    al.add(temp);
                }
                adaptador.notifyDataSetChanged(); //Notificar siempre los cambios que se han producido en el adaptador para su interfaz gráfica
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                //Cargar cursor
                cursor = baseDatos.buscar2(al.get(position).getName());

                informacion1.setText("");
                informacion2.setText("");
                informacion3.setText("");
                informacion4.setText("");

                String Name="";
                String CountryCode="";
                String District="";
                int Population=0;

                //Recoger los datos de la bbdd y añadirlos al arraylist
                for(int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    int id1 = cursor.getInt(0);
                    Name = cursor.getString(1);
                    CountryCode = cursor.getString(2);
                    District = cursor.getString(3);
                    Population = cursor.getInt(4);
                }
                informacion1.setText("Nombre: "+Name);
                informacion2.setText("País: "+CountryCode);
                informacion3.setText("Distrito: "+District);
                informacion4.setText("Población: "+Population);
            }
        });

    }

    private void añadirElementosAL2() {
        //Cargar cursor
        cursor = baseDatos2.cargarCursorPais();
        //Limpiar arraylist para rellenarlo con los datos que se van a recoger de la bbdd.
        al.clear();
        //Recoger los datos de la bbdd y añadirlos al arraylist
        for(int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            String Code = cursor.getString(0);
            String Name = cursor.getString(1);
            String Continent = cursor.getString(2);
            String Region = cursor.getString(3);
            int SurfaceArea = cursor.getInt(4);
            int IndepYear = cursor.getInt(5);
            int Population = cursor.getInt(6);
            int LifeExpectancy = cursor.getInt(7);
            int GNP = cursor.getInt(8);
            int GNPOld = cursor.getInt(9);
            String LocalName = cursor.getString(10);
            String GovernmentForm = cursor.getString(11);
            String HeadOfState = cursor.getString(12);
            int Capital = cursor.getInt(13);
            String Code2 = cursor.getString(14);

            Pais temp = new Pais(Code, Name, Continent, Region, SurfaceArea, IndepYear, Population, LifeExpectancy, GNP, GNPOld, LocalName, GovernmentForm, HeadOfState, Capital, Code2, "country");
            al2.add(temp);
        }
    }


    private void añadirElementosAL() {
        //Cargar cursor
        cursor = baseDatos.cargarCursorCiudad();
        //Limpiar arraylist para rellenarlo con los datos que se van a recoger de la bbdd.
        al.clear();
        //Recoger los datos de la bbdd y añadirlos al arraylist
        for(int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            int id = cursor.getInt(0);
            String Name = cursor.getString(1);
            String CountryCode = cursor.getString(2);
            String District = cursor.getString(3);
            int Population = cursor.getInt(4);

            Ciudad temp = new Ciudad(id, Name, CountryCode, District, Population, "city");
            al.add(temp);
        }
    }

    // Crear Menu contextual al mantener pulsado
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        //Inflador del menú contextual
        MenuInflater inflater=getMenuInflater();

        // Si el componente que vamos a dibujar es el ListView usamos
        // el fichero XML correspondiente
        if (v.getId() == R.id.MiLista) {
            AdapterView.AdapterContextMenuInfo info=(AdapterView.AdapterContextMenuInfo)menuInfo;
            // Definimos la cabecera del menú contextual
            //Cargar cursor
            cursor = baseDatos2.buscar(al.get(info.position).getName());
            String Name="";
            for(int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                Name = cursor.getString(0);
            }
            menu.setHeaderTitle("Datos del país: "+Name); // city countrycode = country code
            // lista.getAdapter().getItem(info.position).toString()
            // Inflamos el menú contextual
            inflater.inflate(R.menu.menu_modificar, menu);
        }
    }

}
