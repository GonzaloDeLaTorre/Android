package com.example.gonzalodelatorre;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class CiudadBDAdapter {
    // Campos de la BD
    public static final String TABLA_BD = "city";

    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_NAME = "Name";
    public static final String CAMPO_COUNTRYCODE = "CountryCode";
    public static final String CAMPO_DISTRICT = "District";
    public static final String CAMPO_POPULATION = "Population";

    public static final String CREATE_TABLE = "create table "+TABLA_BD+" ("+CAMPO_ID+" integer primary key autoincrement, "+CAMPO_NAME+" text, "+CAMPO_COUNTRYCODE+" text, "+CAMPO_DISTRICT+" text, "+CAMPO_POPULATION+" integer);";

    private Context contexto;
    private SQLiteDatabase bd;
    private CiudadBDHelper helper;

    public CiudadBDAdapter(Context context) {
        this.contexto = context;
    }

    public CiudadBDAdapter abrir() throws SQLException {
        helper = new CiudadBDHelper(this.contexto, "world.bd", null, 1);
        bd = helper.getWritableDatabase(); //Muy importante para abrir una bbdd, primero creas la tabla('helper') y despues hay que hacer esta línea
        return null;
    }

    public void cerrar(){
        helper.close();
    }

    // Método que crea un objeto ContentValues con los parámetros indicados
    private ContentValues generarContentValues(String id, String Name, String CountryCode, String District, String Population){
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, id);
        valores.put(CAMPO_NAME, Name);
        valores.put(CAMPO_COUNTRYCODE, CountryCode);
        valores.put(CAMPO_DISTRICT, District);
        valores.put(CAMPO_POPULATION, Population);

        return valores;
    }

    public void insertar(String id, String Name, String CountryCode, String District, String Population){
        bd.insert(TABLA_BD, null, generarContentValues(id, Name, CountryCode, District, Population));
    }

    public void eliminar(String _id){ //Cambiar por long id para el campo_id cuando lo entienda y lo tenga todo
        bd.delete(TABLA_BD, CAMPO_ID+"='"+_id+"'", null);
        //Otra forma de hacerlo...
        // String eliminar = "delete from notas where titulo="+id+";";
        // bd.execSQL(eliminar);
    }

    public Cursor buscar(String name){
        Cursor b = bd.rawQuery("select "+CAMPO_NAME+" from city where "+CAMPO_NAME+" LIKE '%"+name+"%';",null);
        return b;
        //LIKE '%valorrecibido1%'
        //  String buscar = "select "+CAMPO_NAME+" from city where "+CAMPO_NAME+" = '"+name+"';";
    }

    public Cursor buscar2(String name){
        Cursor b = bd.rawQuery("select _id, Name, CountryCode, District, Population from city where "+CAMPO_NAME+" = '"+name+"';",null);
        return b;
        //  String buscar = "select "+CAMPO_NAME+" from city where "+CAMPO_NAME+" = '"+name+"';";
        //  bd.execSQL(buscar);
    }

    //Cargar el cursor para poder hacer un 'select*from nombreTabla;' y así recorrer la bbdd para cualquier utilidad necesaria;
    public Cursor cargarCursorCiudad(){
        Cursor b = bd.rawQuery("SELECT _id, Name, CountryCode, District, Population FROM city;",null);
        return b;
    }


}
