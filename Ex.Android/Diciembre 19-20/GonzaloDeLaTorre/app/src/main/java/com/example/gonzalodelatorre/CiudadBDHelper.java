package com.example.gonzalodelatorre;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class CiudadBDHelper extends SQLiteOpenHelper {
    // Definimos el nombre y la versión de la BD
    private static final String BD_NOMBRE = "world.bd";
    private static final int BD_VERSION = 1;
    private Context contexto;

    // Constructor de la clase
    public CiudadBDHelper(Context context, String nombre, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, BD_NOMBRE, null, BD_VERSION);
        this.contexto = context;
    }

    // Método invocado por Android si no existe la BD
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            // La estructura de la base de datos se define dentro del fichero worldsqliterelacional.sql
            // Cargamos también datos en la base de datos
            InputStream ficheroraw = contexto.getResources().openRawResource(R.raw.worldsqliterelacional);
            BufferedReader in = new BufferedReader(new InputStreamReader(ficheroraw));
            String linea = "";
            while ((linea = in.readLine()) != null){
                db.execSQL(linea);
            }
            in.close();
        } catch (SQLException e) {
            Log.e("Error al crear BD", e.toString());
            throw e;
        } catch (IOException e) {
            Log.e("Error al leer los datos", e.toString());
        }
    }

    // Método invocado por Android si hay un cambio de versión de la BD
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}
