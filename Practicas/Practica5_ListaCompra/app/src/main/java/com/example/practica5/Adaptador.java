package com.example.practica5;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class Adaptador extends ArrayAdapter<Articulo> {

    Activity contexto;
    ArrayList<Articulo> datos;

    public Adaptador(Activity contexto, ArrayList<Articulo> datos) {
        super(contexto, R.layout.elemento_lista, datos);
        this.contexto = contexto;
        this.datos = datos;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = contexto.getLayoutInflater();
        View item = inflater.inflate(R.layout.elemento_lista, null);

        Articulo mielemento=getItem(position);

        TextView titulo = (TextView) item.findViewById(R.id.textView_Alimento);

        if (datos.get(position).isComprado()) {
            titulo.setPaintFlags(titulo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            titulo.setTextColor(Color.parseColor("#00FF00"));
        } else {
            titulo.setPaintFlags(titulo.getPaintFlags() &~Paint.STRIKE_THRU_TEXT_FLAG);
            titulo.setTextColor(Color.parseColor("#FF0000"));
        }

        titulo.setText(mielemento.getNombre());

        return(item);
    }

}
