package com.example.practica5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements EditFragment.EditNameDialogListener {

    ListView lista;
   // TextView nombreArticulo; //  = (TextView) findViewById(R.id.textView_Alimento);

    ArrayList<Articulo> al = new ArrayList<Articulo>();
    ArrayList<String> al2 = new ArrayList<String>();

    Adaptador adaptador;

    private FragmentManager fm = getSupportFragmentManager();   //Para sacar el Edit Dialog Fragment

    AdapterView.AdapterContextMenuInfo info; //Instanciar mediante AdapterView el adaptador del Menu contextual

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = (ListView) findViewById(R.id.MiLista);

      //  nombreArticulo= (TextView) findViewById(R.id.textView_Alimento);

        añadirElementosAL();

        adaptador = new Adaptador(this, al);

        lista.setAdapter(adaptador);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (al.get(position).isComprado()) {
                    al.get(position).setComprado(false);
                    Toast.makeText(getApplicationContext(), "No has comprado "+al.get(position).getNombre(),Toast.LENGTH_LONG).show();
                } else {
                    al.get(position).setComprado(true);
                    Toast.makeText(getApplicationContext(), "Has comprado "+al.get(position).getNombre(),Toast.LENGTH_LONG).show();
                }
                adaptador.notifyDataSetChanged();
            }
        });
        //Registrar el Menu contextual
        registerForContextMenu(lista);

    }

    // Crear Menu contextual al mantener pulsado
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        //Inflador del menú contextual
        MenuInflater inflater=getMenuInflater();

        // Si el componente que vamos a dibujar es el ListView usamos
        // el fichero XML correspondiente
        if (v.getId() == R.id.MiLista) {
            AdapterView.AdapterContextMenuInfo info=(AdapterView.AdapterContextMenuInfo)menuInfo;
            // Definimos la cabecera del menú contextual
            menu.setHeaderTitle(al.get(info.position).getNombre());
            // lista.getAdapter().getItem(info.position).toString()
            // Inflamos el menú contextual
            inflater.inflate(R.menu.menu_modificar, menu);
        }

    }

    // Dar valor al Menu contextual
    public boolean onContextItemSelected(MenuItem item) {
        this.info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if (item.getItemId() == R.id.EditarTexto) {
            EditFragment eF2 = new EditFragment();
            String nombreArticuloAEditar=((Articulo) this.al.get(this.info.position)).getNombre();
            Bundle bundle = new Bundle(2);
            bundle.putString("nombreArticuloAEditar", nombreArticuloAEditar);
            bundle.putInt("posicionAEditar", this.info.position);
            eF2.setArguments(bundle);
            eF2.show(this.fm, "Edita articulo");
        }

        if (item.getItemId() == R.id.EliminarTexto) {
            al.remove(info.position);
        }

        adaptador.notifyDataSetChanged();
        return super.onContextItemSelected(item);
    }

    // Inflar Menu AñadirArticulo del action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // Crear Menu AñadirArticulo
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        EditFragment eF = new EditFragment();
        eF.show(fm, "Añadir artículo");

        adaptador.notifyDataSetChanged();

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String nombreArticulo, int posicionArticuloEditar) {
        if (nombreArticulo.length()==0 || nombreArticulo == null) {
            Toast.makeText(getApplicationContext(), "Alta no realizada.\nMotivo: Nombre vacío",Toast.LENGTH_LONG).show();
        } else {
            if (posicionArticuloEditar == -1) {
                al.add(new Articulo(nombreArticulo,false));
                adaptador.notifyDataSetChanged();
            }
        }
    }

    public void onDialogPositiveClick2(DialogFragment dialog, String nombreArticulo, int posicionArticuloEditar) {
        if (nombreArticulo.length()==0 || nombreArticulo == null) {
            Toast.makeText(getApplicationContext(), "Modificación no realizada.\nMotivo: Nombre vacío",Toast.LENGTH_LONG).show();
        } else {
            if (posicionArticuloEditar == -1) {
                al.get(info.position).setNombre(nombreArticulo);
                adaptador.notifyDataSetChanged();
            }
        }
    }

    private void añadirElementosAL() {
        Articulo op1 = new Articulo("Morcilla", true);
        al.add(op1);
        Articulo op2 = new Articulo("Queso", false);
        al.add(op2);
        Articulo op3 = new Articulo("Frutos secos", true);
        al.add(op3);
    }



}