package com.example.practica5;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

public class EditFragment extends DialogFragment {

    private EditNameDialogListener mListener;

    public interface EditNameDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, String nombreArticulo, int posicionArticuloEditar);
        public void onDialogPositiveClick2(DialogFragment dialog, String nombreArticulo, int posicionArticuloEditar);
    }

    public EditFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = ( EditNameDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement EditNameDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int posicion=-1;
        String nombreArticuloAEditar;

        // Crear pantallita de nuevo articulo
        final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());

        //Modificar
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View v = inflater.inflate(R.layout.dialogfragment,null);
        final EditText articulo = (EditText) v.findViewById(R.id.InputNombreArticulo);


        if (getArguments() == null) { //ALTA
            adb.setIcon(R.drawable.ic_add_black_24dp);
            adb.setTitle("Alta Artículo");
            adb.setMessage("Introduce un nuevo artículo:");
            adb.setView(v);

            adb.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mListener.onDialogPositiveClick(EditFragment.this, articulo.getText().toString(), posicion);
                }
            });

            adb.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(adb.getContext(), "Alta cancelada", Toast.LENGTH_LONG).show();
                }
            });

        } else { //MODIFICAR
            nombreArticuloAEditar = getArguments().getString("nombreArticuloAEditar");
            adb.setTitle("Modificar Artículo");
            adb.setMessage("Modifica el artículo:");
            adb.setView(v);
            articulo.setText(nombreArticuloAEditar);
            articulo.setSelectAllOnFocus(true);

            adb.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mListener.onDialogPositiveClick2(EditFragment.this, articulo.getText().toString(), posicion);
                }
            });

            adb.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(adb.getContext(), "Modificación cancelada", Toast.LENGTH_LONG).show();
                }
            });
        }
        return adb.create();
    }

}
