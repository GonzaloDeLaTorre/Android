package com.example.practica10_gestornotas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import static com.example.practica10_gestornotas.NotasBDAdapter.TABLA_BD;

public class MainActivity extends AppCompatActivity {

    FloatingActionButton añadir;
    TextView titulo;
    ImageView foto;

    private NotasBDAdapter baseDatos;
    private Cursor cursor;
    private ListView lista;
    Adaptador adaptador;
    ArrayList<Nota> al;
    AdapterView.AdapterContextMenuInfo info; //Instanciar mediante AdapterView el adaptador del Menu contextual

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        añadir = (FloatingActionButton) findViewById(R.id.Boton_Añadir);
        titulo = (TextView) findViewById(R.id.TextView_Vacio);
        foto = (ImageView) findViewById(R.id.imageView_Icono);
        lista = (ListView) findViewById(R.id.Lista);

        //Crear la BBDD
        baseDatos = new NotasBDAdapter(this);
        //Abrir la BBDD (Se puede abrir directamente al crearla, pero yo he decidido abrirlo desde un metodo)
        baseDatos.abrir();

        //Crear ArrayList de las Notas
        al = new ArrayList<Nota>();
        //Crear Adaptador
        adaptador = new Adaptador(this, al);
        //'Adaptar' la Lista al Adaptador con sus valores
        lista.setAdapter(adaptador);
        //Registrar el Menu contextual
        registerForContextMenu(lista);
        //Añadir elementos al ArrayList Notas
        añadirElementosAL();
        //Actualizar siempre la Lista cada vez que se hace algo en la app
        actualizarLista();

        //Editar la nota ya creada
        editarNota();

        //Condicion limpiar textView inicial
        if (al.size()>0) {
            titulo.setText("");
        }

    }

    private void editarNota() {
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent siguiente = new Intent(MainActivity.this, SegundoActivity.class);

                //Guardar valores para enviar al SegundoActivity y recogerlo en los Strings
                siguiente.putExtra("datoCategoria", al.get(position).getCategoria());
                siguiente.putExtra("datoTitulo", al.get(position).getTitulo());
                siguiente.putExtra("datoDescripcion", al.get(position).getDescripcion());

                //Iniciar Intent (En este caso ir al SegundoActivity)
                startActivity(siguiente);
            }
        });
    }

    private void actualizarLista() {
        //Cargar cursor
        cursor = baseDatos.cargarCursorNotas();
        //Limpiar arraylist para rellenarlo con los datos que se van a recoger de la bbdd.
        al.clear();
        //Recoger los datos de la bbdd y añadirlos al arraylist
        for(int i = 0; i < cursor.getCount(); i++) {
            cursor.moveToPosition(i);
            String id = cursor.getString(0);
            String categoria = cursor.getString(1);
            String titulo = cursor.getString(2);
            String descripcion = cursor.getString(3);
            String icono = cursor.getString(4);

            Nota temp = new Nota(id, categoria, titulo, descripcion, icono, "notas");
            al.add(temp);

            Toast.makeText(getApplicationContext(), ""+icono,Toast.LENGTH_SHORT).show();
        }
    }


    private void añadirElementosAL() {
        //Recoger dato recibido del intent del SegundoActivity ('anterior')
        String d_Id = getIntent().getStringExtra("datoId");
        String d_Categoria = getIntent().getStringExtra("datoCategoria");
        String d_Titulo = getIntent().getStringExtra("datoTitulo");
        String d_Descripcion = getIntent().getStringExtra("datoDescripcion");
        String d_Icono = getIntent().getStringExtra("datoIcono");

        if (d_Titulo != null) {
            //Vaciar TextView inicial
            titulo.setText("");
            //Insertar linea en la bbdd
            baseDatos.insertar(d_Id, d_Categoria, d_Titulo, d_Descripcion, d_Icono);
            //Actualizar adaptador
            adaptador.notifyDataSetChanged();
        }
    }


    // Crear Menu contextual al mantener pulsado
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        //Inflador del menú contextual
        MenuInflater inflater=getMenuInflater();

        // Si el componente que vamos a dibujar es el ListView usamos
        // el fichero XML correspondiente
        if (v.getId() == R.id.Lista) {
            AdapterView.AdapterContextMenuInfo info=(AdapterView.AdapterContextMenuInfo)menuInfo;
            // Definimos la cabecera del menú contextual
            menu.setHeaderTitle(al.get(info.position).getTitulo());
            // lista.getAdapter().getItem(info.position).toString()
            // Inflamos el menú contextual
            inflater.inflate(R.menu.menu_modificar, menu);
//            menu.getItem(0);
        }
    }

    // Dar valor al Menu contextual
    public boolean onContextItemSelected(MenuItem item) {
        this.info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if (item.getItemId() == R.id.EliminarNota) {
            baseDatos.eliminar(al.get(info.position).getTitulo()); //Primero borrar de la base de datos, depsues del arraylist
            al.remove(al.get(info.position));
        }
        adaptador.notifyDataSetChanged(); //Notificar siempre los cambios que se han producido en el adaptador para su interfaz gráfica
        return super.onContextItemSelected(item);
    }

    // Crear segunda actividad
    public void añadir(View v){
        //Crear Intent
        Intent siguiente = new Intent(this, SegundoActivity.class);

        //Iniciar Intent (En este caso ir al SegundoActivity)
        startActivity(siguiente);
    };

}
