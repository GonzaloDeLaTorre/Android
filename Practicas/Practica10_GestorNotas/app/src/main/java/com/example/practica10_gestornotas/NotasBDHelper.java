package com.example.practica10_gestornotas;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class NotasBDHelper extends SQLiteOpenHelper {
    // Definimos el nombre y la versión de la BD
    private static final String BD_NOMBRE = "bdnotas.bd";
    private static final int BD_VERSION = 1;

    // Constructor de la clase
    public NotasBDHelper(Context context, String nombre, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, BD_NOMBRE, null, BD_VERSION);
    }

    // Método invocado por Android si no existe la BD
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(NotasBDAdapter.CREATE_TABLE);
    }

    // Método invocado por Android si hay un cambio de versión de la BD
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS notas");
        db.execSQL(NotasBDAdapter.CREATE_TABLE);
    }

}
