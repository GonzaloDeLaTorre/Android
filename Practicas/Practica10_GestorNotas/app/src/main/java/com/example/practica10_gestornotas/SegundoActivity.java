package com.example.practica10_gestornotas;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class SegundoActivity extends AppCompatActivity {

    Button aceptar;

    Spinner spiner;
    EditText titulo;
    EditText descripcion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segundo);

        aceptar = (Button) findViewById(R.id.button_Aceptar);
        spiner = (Spinner) findViewById(R.id.spinner);
        titulo = (EditText) findViewById(R.id.editText_Titulo);
        descripcion = (EditText) findViewById(R.id.editText_EscribeDescripcion);

        //Recoger dato recibido del intent del MainActivity ('siguiente')
        String d_Categoria = getIntent().getStringExtra("datoCategoria");
        String d_Titulo = getIntent().getStringExtra("datoTitulo");
        String d_Descripcion = getIntent().getStringExtra("datoDescripcion");
        //Esta condicion es para cuando se inicia la app y se crea la primera nota, ya que no recibe nada del intent 'siguiente' y salta excepcion vacía.
        if (d_Categoria!=null) {
            if (d_Categoria.equals(R.array.array_name==0)) { // "REUNIÓN"
                spiner.setSelection(0);
            }
            if (d_Categoria.equals(R.array.array_name==1)) { // "AVISO"
                spiner.setSelection(1);
            }
            if (d_Categoria.equals(R.array.array_name==2)) { // "VARIOS"
                spiner.setSelection(2);
            }
        }

        titulo.setText(d_Titulo);
        descripcion.setText(d_Descripcion);
    }

    public void aceptar(View v){
        //Crear Intent
        Intent anterior = new Intent(this, MainActivity.class);

        //Guardar informacion para enviar al primer activity
        anterior.putExtra("datoCategoria", spiner.getSelectedItem().toString());
        anterior.putExtra("datoTitulo", titulo.getText().toString());
        anterior.putExtra("datoDescripcion", descripcion.getText().toString());

        //Iniciar Intent (En este caso ir al MainActivity)
        startActivity(anterior);
    };
}
