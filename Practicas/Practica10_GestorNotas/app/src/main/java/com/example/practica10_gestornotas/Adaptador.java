package com.example.practica10_gestornotas;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adaptador extends ArrayAdapter<Nota> {

    Activity contexto;
//    ArrayList<Nota> datos;

    public Adaptador(Activity contexto, ArrayList<Nota> datos) {
        super(contexto, R.layout.elemento_lista, datos);
        this.contexto = contexto;
//        this.datos = datos;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = contexto.getLayoutInflater();
        View item = inflater.inflate(R.layout.elemento_lista, null);

        Nota mielemento=getItem(position);

        TextView titulo = (TextView) item.findViewById(R.id.textView_Titulo);
        TextView tipo = (TextView) item.findViewById(R.id.textView_Tipo);
        ImageView foto = (ImageView) item.findViewById(R.id.imageView_Icono);

        titulo.setText(mielemento.getTitulo());
        tipo.setText(mielemento.getCategoria());
        if (mielemento.getCategoria().equals("REUNIÓN")) {
            foto.setImageResource(R.drawable.calendario);
        }
        if (mielemento.getCategoria().equals("AVISO")) {
            foto.setImageResource(R.drawable.aviso);
        }
        if (mielemento.getCategoria().equals("VARIOS")) {
            foto.setImageResource(R.drawable.varios);
        }

        return(item);
    }

}
