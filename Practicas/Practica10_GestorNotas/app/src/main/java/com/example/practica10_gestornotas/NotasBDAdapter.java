package com.example.practica10_gestornotas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class NotasBDAdapter {
    // Campos de la BD
    public static final String TABLA_BD = "notas";

    public static final String CAMPO_ID = "_id";
    public static final String CAMPO_CATEGORIA = "categoria";
    public static final String CAMPO_TITULO = "titulo";
    public static final String CAMPO_DESCRIPCION = "descripcion";
    public static final String CAMPO_ICONO = "icono";

    public static final String CREATE_TABLE = "create table "+TABLA_BD+" ("+CAMPO_ID+" integer primary key autoincrement, "+CAMPO_CATEGORIA+" text, "+CAMPO_TITULO+" text, "+CAMPO_DESCRIPCION+" text, "+CAMPO_ICONO+" integer);";

    private Context contexto;
    private SQLiteDatabase bd;
    private NotasBDHelper helper;

    public NotasBDAdapter(Context context) {
        this.contexto = context;
    }

    public NotasBDAdapter abrir() throws SQLException {
        helper = new NotasBDHelper(this.contexto, "bdnotas.bd", null, 1);
        bd = helper.getWritableDatabase(); //Muy importante para abrir una bbdd, primero creas la tabla('helper') y despues hay que hacer esta línea
        return null;
    }

    public void cerrar(){
        helper.close();
    }

    // Método que crea un objeto ContentValues con los parámetros indicados
    private ContentValues generarContentValues(String id, String categoria, String titulo, String descripcion, String icono){
        ContentValues valores = new ContentValues();
        valores.put(CAMPO_ID, id);
        valores.put(CAMPO_CATEGORIA, categoria);
        valores.put(CAMPO_TITULO, titulo);
        valores.put(CAMPO_DESCRIPCION, descripcion);
        valores.put(CAMPO_ICONO, icono);

        return valores;
    }

    public void insertar(String id, String categoria, String titulo, String descripcion, String icono){
        bd.insert(TABLA_BD, null, generarContentValues(id, categoria, titulo, descripcion, icono));
    }

    public void eliminar(String titulo){ //Cambiar por long id para el campo_id cuando lo entienda y lo tenga todo
        bd.delete(TABLA_BD, CAMPO_TITULO+"='"+titulo+"'", null);
        //Otra forma de hacerlo...
        // String eliminar = "delete from notas where titulo="+id+";";
        // bd.execSQL(eliminar);
    }

    public void modificar(String id, String categoria, String titulo, String descripcion, String icono){
        bd.update(TABLA_BD, generarContentValues(id, categoria, titulo, descripcion, icono), "categoria='"+categoria+"'", new String[]{categoria});
        // String modificar = "UPDATE notas set "+categoria+" = "+categoria+" where titulo="+id+";";
        // bd.execSQL(eliminar);
    }

    //Cargar el cursor para poder hacer un 'select*from nombreTabla;' y así recorrer la bbdd para cualquier utilidad necesaria;
    public Cursor cargarCursorNotas(){
        Cursor b = bd.rawQuery("SELECT _id, categoria, titulo, descripcion, icono FROM notas;",null);
        return b;
    }

}
