package com.example.practica7_enviarperfil;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import com.google.android.material.snackbar.Snackbar;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {

    //Permisos
    private static int MY_WRITE_EXTERNAL_STORAGE = 0;
    private String comments = null;
    private View mLayout;
    int permisosAceptados=0;
    EditText editText;

    ImageView fotoAvatar;
    Spinner spinner;

    //Instanciar para crear la barra de progreso
    private ProgressDialog miProgresoDialog; //Barra de progreso
    private int miProgreso; //Control del progreso
    private static final int PROGRESO_MAX = 100; //Progreso maximo
    private Handler miProgresoHandler; //Controlador para simular el cambio del progreso

    //Cargar imagen
    private static int RESULT_LOAD_IMAGE = 1 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLayout = findViewById(R.id.relativeLayoutMain);
        editText = (EditText) findViewById(R.id.editText_EscribeDescripcion);

        fotoAvatar = (ImageView) findViewById(R.id.imageView_Imagen);
        fotoAvatar.setImageResource(R.drawable.avatar);

        spinner = (Spinner) findViewById(R.id.spinner);
    }


    //Paso 1 : Verificar permisos, si no los tiene entra a pedirlos
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void verifyPermission() {
        //WRITE_EXTERNAL_STORAGE tiene implícito READ_EXTERNAL_STORAGE
        //porque pertenecen al mismo grupo de permisos
        int writePermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (writePermission != PackageManager.PERMISSION_GRANTED){
            requestPermission();
        }
        else {
            saveComments();
        }
    }

    //Paso 2 : Solicitar los permisos
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showSnackBar();
        }else {
            requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_WRITE_EXTERNAL_STORAGE);
        }
    }

    //Paso 3 : Procesar respuesta del usuario
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_WRITE_EXTERNAL_STORAGE) { //SÍ
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                entrarGaleria();
                permisosAceptados=1;
                saveComments();
            }else { //NO
                showSnackBar();
            }
        }
    }


    /**
     * Método para mostrar el snackbar de la aplicación.
     * Snackbar es un componente de la librería de diseño 'com.android.support:design:23.1.0'
     * y puede ser personalizado para realizar una acción, como por ejemplo abrir la actividad de
     * configuración de nuestra aplicación.
     */
    private void showSnackBar() {
        Snackbar.make(mLayout, R.string.permission_write_storage, Snackbar.LENGTH_LONG).setAction(R.string.settings, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSettings();
            }
            }).show();
    }

    /**
     * Abre el intento de detalles de configuración de nuestra aplicación
     */
    public void openSettings() {
        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
    }

    /**
     * Guarda el comentario
     */
    private void saveComments() {

        if (isExternalStorageWritable()) {
            try {
                File file = new File(Environment.getExternalStorageDirectory(), "comments.aee");
                boolean created = file.createNewFile();
                if (file.exists()) {
                    OutputStream fo = new FileOutputStream(file, true);
                    fo.write(comments.getBytes());
                    fo.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();

        return Environment.MEDIA_MOUNTED.equals(state);
    }




    //Cargar Imagen de contacto
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            //Coger la imagen seleccionada
            Uri selectedImage = data.getData();

            fotoAvatar.setImageURI(selectedImage);
        }
    }


    //Boton seleccionar
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void seleccionar (View v) {

        verifyPermission();
        comments = editText.getText().toString();

        if (permisosAceptados==1) {
            entrarGaleria();
        }

    }

    private void entrarGaleria() {
        Intent i = new Intent( Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        i.setType("image/*");
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    //Boton enviar
    public void enviar (View v){
        //Dar valor a lo instanciado previamente
        miProgresoDialog = new ProgressDialog(MainActivity.this);
        miProgreso = 0;

        //Modificar las variables y atributos
        miProgresoDialog.setProgress(0); //Poner la barra de progreso a cero
        miProgresoDialog.setIcon(android.R.drawable.ic_dialog_info);
        miProgresoDialog.setTitle("Enviando...");
        miProgresoDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        miProgresoDialog.setMax(PROGRESO_MAX); //Definir el maximo que puede alcanzar la barra de progreso

        //Crear boton cancelar justo debajo de la barra de progreso
        miProgresoDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
                "Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int boton) {
                        Toast.makeText(MainActivity.this, "Se ha cancelado el envío", Toast.LENGTH_SHORT).show();
                    }
                });

        //Mostrar la barra de progreso
        miProgresoDialog.show();

        //Crear el controlador que simula el cambio en el progreso. Es como un temporizador que usamos para dibujar la barra de progreso en una ventana de dialogo.
        miProgresoHandler = new Handler() {
            @Override
            public void handleMessage(Message msj) {
                super.handleMessage(msj);
                if (miProgreso >= PROGRESO_MAX) { //Si 'miProgreso' que inicia en 0, es mayor o igual que el 'PROGRESO_MAX' que es 100, se finaliza la ventana de dialogo.
                    miProgresoDialog.dismiss(); //Finalizar ventana de dialogo
                    Toast.makeText(MainActivity.this, "Se ha enviado correctamente", Toast.LENGTH_SHORT).show();
                    reiniciar();
                } else {
                    miProgreso++; //Aumenta 'miProgreso'(variable int) de 1 en 1
                    miProgresoDialog.incrementProgressBy(1); //Aumenta 'miProgresoDialog'(barra de progreso) de 1 en 1
                    miProgresoHandler.sendEmptyMessageDelayed(0, 100); //Aumenta 'miProgresoHandler'(dibujo barra de progreso) desde 0 hasta 100
                    // Si se sale de la barra de estado sin pulsar el boton 'cancelar'
                    if (!miProgresoDialog.isShowing()) {
                        miProgresoHandler.removeCallbacksAndMessages(null);
                        Toast.makeText(MainActivity.this, "Se ha cancelado el envío", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        miProgresoHandler.sendEmptyMessage(0); //NI IDEA
    }

    private void reiniciar() {
        fotoAvatar.setImageResource(R.drawable.avatar);
        spinner.setSelection(0);
        editText.setText("");
    }

}
