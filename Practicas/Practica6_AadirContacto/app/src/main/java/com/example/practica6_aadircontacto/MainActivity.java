package com.example.practica6_aadircontacto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button botonAlta;
    Button botonModificar;
    TextView contactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonAlta = (Button) findViewById(R.id.button_Alta);
        botonModificar = (Button) findViewById(R.id.button_Modificar);
        contactos = (TextView) findViewById(R.id.textView_Contactos);

        botonAlta.setEnabled(true);
        botonModificar.setEnabled(false);

        String datoNombre = getIntent().getStringExtra("datoNombre");
        String datoApellidos = getIntent().getStringExtra("datoApellido");

        contactos.setText("Nombre: "+datoNombre+"\nApellidos: "+datoApellidos);
    }

    public void pulsar(View v){
        Intent siguiente = new Intent(this, SegundoActivity.class);



        startActivity(siguiente);
    };

}
