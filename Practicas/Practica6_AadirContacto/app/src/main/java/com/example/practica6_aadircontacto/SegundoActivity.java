package com.example.practica6_aadircontacto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class SegundoActivity extends AppCompatActivity {

    EditText nombre;
    EditText apellidos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segundo);

        nombre = (EditText) findViewById(R.id.editText_Nombre);
        apellidos = (EditText) findViewById(R.id.editText_Apellidos);
    }

    public void pulsar(View v){
        Intent anterior = new Intent(this, MainActivity.class);

        anterior.putExtra("datoNombre", nombre.getText().toString());
        anterior.putExtra("datoApellido", apellidos.getText().toString());

        startActivity(anterior);
    };
}
