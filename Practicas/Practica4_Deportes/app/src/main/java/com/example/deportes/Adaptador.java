package com.example.deportes;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adaptador extends ArrayAdapter<Opcion> {

    Activity contexto;

    public Adaptador(Activity contexto, ArrayList<Opcion> datos) {
        super(contexto, R.layout.elemento_lista, datos);
        this.contexto = contexto;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = contexto.getLayoutInflater();
        View item = inflater.inflate(R.layout.elemento_lista, null);

        Opcion mielemento=getItem(position);

        ImageView imagen = (ImageView) item.findViewById(R.id.imageView_Imagen);
        TextView titulo = (TextView) item.findViewById(R.id.textView_Nombre);
        CheckBox tick = (CheckBox) item.findViewById(R.id.checkBox_Tick);

        imagen.setImageResource(mielemento.getIcono());
        titulo.setText(mielemento.getNombre());
        tick.setChecked(mielemento.getCheckbox());

        return(item);
    }

}
