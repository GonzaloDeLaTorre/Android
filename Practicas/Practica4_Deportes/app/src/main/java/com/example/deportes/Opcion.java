package com.example.deportes;

public class Opcion {

    String nombre;
    int icono;
    boolean tick;

    public Opcion(int icono, String nombre, boolean tick) {
        this.icono = icono;
        this.nombre = nombre;
        this.tick = tick;
    }

    public boolean getCheckbox(){
        return this.tick;
    }
    public void setCheckbox(boolean tick){
        this.tick = tick;
    }

    public String getNombre() {
        return this.nombre;
    }

    public int getIcono() {
        return this.icono;
    }

}
