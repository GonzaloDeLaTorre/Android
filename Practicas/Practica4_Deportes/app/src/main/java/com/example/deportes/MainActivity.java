package com.example.deportes;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lista;

    ArrayList<Opcion> al = new ArrayList<Opcion>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = (ListView) findViewById(R.id.MiLista);

        final Adaptador adaptador = new Adaptador(this, al);

        Opcion op1 = new Opcion(R.drawable.baloncesto, getResources().getString(R.string.baloncesto), false);
        al.add(op1);
        Opcion op2 = new Opcion(R.drawable.futbol, getResources().getString(R.string.futbol), false);
        al.add(op2);
        Opcion op3 = new Opcion(R.drawable.motociclismo, getResources().getString(R.string.motociclismo), false);
        al.add(op3);
        Opcion op4 = new Opcion(R.drawable.natacion, getResources().getString(R.string.natacion), false);
        al.add(op4);
        Opcion op5 = new Opcion(R.drawable.golf, getResources().getString(R.string.golf), false);
        al.add(op5);
        Opcion op6 = new Opcion(R.drawable.atletismo, getResources().getString(R.string.atletismo), false);
        al.add(op6);
        Opcion op7 = new Opcion(R.drawable.pingpong, getResources().getString(R.string.pingPong), false);
        al.add(op7);

        lista.setAdapter(adaptador);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (al.get(position).getCheckbox()==false) {
                    al.get(position).setCheckbox(true);
                } else {
                    al.get(position).setCheckbox(false);
                }
                System.out.println(al.get(position).getCheckbox());
                adaptador.notifyDataSetChanged();
            }
        });

    }

    public void pulsar(View v){
        int cnt = 0;
        String linea="";
        for (int i = 0; i < al.size(); i++) {
            if (al.get(i).getCheckbox()==true) {
                if (cnt==0) {
                    linea=linea+" "+getResources().getStringArray(R.array.deportes)[i];
                }
                if (cnt>0) {
                    linea=linea+", "+getResources().getStringArray(R.array.deportes)[i];
                }
                cnt++;
            }
        }
        if (cnt>0) {
            Toast.makeText(getApplicationContext(), "Te gusta" + linea,Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "No has seleccionado ninguna opción",Toast.LENGTH_LONG).show();
        }
    };

}
