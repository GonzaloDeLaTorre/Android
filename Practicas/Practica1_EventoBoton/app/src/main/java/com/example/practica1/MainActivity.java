package com.example.practica1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Notification;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Instanciar paleta*/
        Button boton = (Button) findViewById(R.id.boton);
        final TextView distancia = (TextView) findViewById(R.id.distancia);
        final EditText introduce = (EditText) findViewById(R.id.introduce);
        final RadioButton radio0 = (RadioButton) findViewById(R.id.radio0);
        final RadioButton radio1 = (RadioButton) findViewById(R.id.radio1);

        boton.setOnClickListener(new View.OnClickListener() { //Dar vida al boton
            @Override
            public void onClick(View view) { //Dar vida al boton
                float km=0;
                float m=0;
                DecimalFormat dosDecimales=new DecimalFormat("0.00");
                if (introduce.getText().length()==0) {
                    Toast.makeText(MainActivity.this, "Por favor, introduce un número", Toast.LENGTH_SHORT).show();
                } else {
                    if (radio0.isChecked()) {
                        m=Float.valueOf(introduce.getText().toString());
                        km=m*1.609f; //cast= (float) (m*1.609)  ó  m*1.609f
                        distancia.setText("Millas son "+dosDecimales.format(km)+" Kms");
                    }

                    if (radio1.isChecked()) {
                        km=Float.valueOf(introduce.getText().toString());
                        m=km/1.609f;
                        distancia.setText("Kms son "+dosDecimales.format(m)+" Millas");
                    }
                }
            }
        });
    }

}
