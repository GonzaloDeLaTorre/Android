package com.example.practica2_adivinaminumero;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView etiquetaSuperior;
    TextView intentalo;
    EditText numero;
    Button probar;
    int cnt=0;
    Random dado=new Random();
    int numeroAAdivinar = dado.nextInt((100) + 1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etiquetaSuperior = findViewById(R.id.etiquetaSuperior);
        intentalo = findViewById(R.id.intentalo);
        numero = findViewById(R.id.numero);
        probar = findViewById(R.id.probar);

        probar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (probar.getText().equals("JUGAR OTRA VEZ")) {
                    numeroAAdivinar = dado.nextInt(100) + 1;
                    etiquetaSuperior.setText("He pensado un número entre 1 y 100.\n¡Adivina cual es!");
                    intentalo.setText("¡Inténtalo!");
                    numero.getText().clear();
                    numero.setVisibility(View.VISIBLE);
                    probar.setText("PROBAR");
                    cnt=0;
                }
                if (numero.getText().toString().matches("^[1-9][0-9]?$|^100$")) {
                    quiz();
                }
            }

            private void quiz() {
                if (Integer.valueOf(numero.getText().toString()) > numeroAAdivinar) {
                    etiquetaSuperior.setText("¿"+numero.getText()+"? ¡Uy! El número que he pensado es menor.");
                }
                if (Integer.valueOf(numero.getText().toString()) < numeroAAdivinar) {
                    etiquetaSuperior.setText("¿"+numero.getText()+"? ¡Uy! El número que he pensado es mayor.");
                }
                cnt= cnt+1;
                intentalo.setText("Llevas "+ cnt);
                if (Integer.valueOf(numero.getText().toString()) == numeroAAdivinar) {
                    etiquetaSuperior.setText("¡¡Has acertado!! ¡¡Y sólo en "+cnt+" intentos!!");
                    numero.setVisibility(View.INVISIBLE);
                    probar.setText("JUGAR OTRA VEZ");
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outInstance) {
        super.onSaveInstanceState(outInstance);

        outInstance.putString("STATE_ETIQUETA_SUPERIOR", etiquetaSuperior.getText().toString());
        outInstance.putString("STATE_INTENTALO", intentalo.getText().toString());
        outInstance.putString("STATE_NUMERO", numero.getText().toString());
        outInstance.putString("STATE_PROBAR", probar.getText().toString());
        outInstance.putInt("STATE_CNT", cnt);
        outInstance.putInt("STATE_RANDOM", numeroAAdivinar);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        etiquetaSuperior.setText(savedInstanceState.getString("STATE_ETIQUETA_SUPERIOR"));
        intentalo.setText(savedInstanceState.getString("STATE_INTENTALO"));
        numero.setText(savedInstanceState.getString("STATE_NUMERO"));
        probar.setText(savedInstanceState.getString("STATE_PROBAR"));
        cnt = savedInstanceState.getInt("STATE_CNT");
        numeroAAdivinar = savedInstanceState.getInt("STATE_RANDOM");
    }
}
