package com.example.practica8_especiessubacuaticas;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

public class Adaptador extends ArrayAdapter<ListaPeces> {

    Activity contexto;

    public Adaptador(Activity contexto, ArrayList<ListaPeces> datos) {
        super(contexto, R.layout.elemento_lista,datos);
        this.contexto = contexto;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = contexto.getLayoutInflater();
        View item = inflater.inflate(R.layout.elemento_lista, null);

        ListaPeces mielemento=getItem(position);

        ImageView imagen = (ImageView) item.findViewById(R.id.imageView_Imagen);
        TextView texto = (TextView) item.findViewById(R.id.textView_Informacion);

        imagen.setImageResource(mielemento.getImagen());
        texto.setText(mielemento.getTexto());

        return(item);
    }

}
