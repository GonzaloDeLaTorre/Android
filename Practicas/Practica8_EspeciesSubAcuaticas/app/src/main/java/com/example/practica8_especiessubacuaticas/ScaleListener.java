package com.example.practica8_especiessubacuaticas;

import android.view.ScaleGestureDetector;
import android.widget.ImageView;

public class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener { // Clase para ampliar imágenes

    ImageView imagen;
    float xBegin;
    float yBegin;
    float scale = 1f;

    public ScaleListener(ImageView imagen, float xBegin, float yBegin){
        this.imagen = imagen;
        this.xBegin = xBegin;
        this.yBegin = yBegin;
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        //Obtenemos la escala de la imagen, la multiplicamos y la asignamos a la variable float scale
        scale *= detector.getScaleFactor();
        //Asignamos dicha escala a la imagen
        if (scale >= 1f) { //Limitamos el reducir imagen a su estado inicial
            imagen.setScaleX(scale);
            imagen.setScaleY(scale);
        }

        return true;
    }
}
