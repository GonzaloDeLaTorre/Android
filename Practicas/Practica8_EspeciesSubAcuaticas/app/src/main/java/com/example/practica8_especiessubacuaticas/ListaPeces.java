package com.example.practica8_especiessubacuaticas;

public class ListaPeces {

    private String texto;
    private int imagen;
    String nombre;
    String nombreLatin;

    public ListaPeces(String texto, int imagen, String nombre, String nombreLatin) {
        this.texto = texto;
        this.imagen = imagen;
        this.nombre = nombre;
        this.nombreLatin = nombreLatin;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreLatin() {
        return nombreLatin;
    }

    public void setNombreLatin(String nombreLatin) {
        this.nombreLatin = nombreLatin;
    }

}
