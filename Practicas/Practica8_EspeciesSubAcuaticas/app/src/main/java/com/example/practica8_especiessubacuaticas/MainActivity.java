package com.example.practica8_especiessubacuaticas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lista;
    Spinner spin;
    TextView titulo;

    ArrayList<ListaPeces> al1 = new ArrayList<ListaPeces>(); //ArrayList para los Peces
    ArrayList<ListaPeces> al2 = new ArrayList<ListaPeces>(); //ArrayList para las Algas

    ArrayAdapter spinAdaptador; //ArrayAdapter para el spinner
    Adaptador adaptadorPeces; //Adaptador para los Peces
    Adaptador adaptadorAlgas; //Adaptador para las Algas

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = (ListView) findViewById(R.id.Lista);
        spin = (Spinner) findViewById(R.id.spinner);
        titulo = (TextView) findViewById(R.id.textView_Parque);

        //Doy valor al spinner con el ArrayAdapter spinAdaptador
        spinAdaptador = ArrayAdapter.createFromResource(this, R.array.array_name, R.layout.support_simple_spinner_dropdown_item); //Parámetro dos: array de items del spinner (strings.xml); Parámetro tres: forma del spinner (edición)
        spin.setAdapter(spinAdaptador);

        //Añado la información a los arrays al1 y al2
        añadirElementosPeces();
        añadirElementosAlgas();

        //Cuando pulsamos las opciones del spinner...
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> listaAdaptador, View view, int pos, long id) {

                //Doy valor a los adaptadores con sus respectivos arrays para los listView
                adaptadorPeces = new Adaptador(MainActivity.this, al1);
                adaptadorAlgas = new Adaptador(MainActivity.this, al2);

                if (listaAdaptador.getItemAtPosition(pos).toString().equals("Peces")) { //Cuando la posición del spinner es cero.
                    titulo.setText("Peces del Parque");
                    lista.setAdapter(adaptadorPeces);
                } else {
                    titulo.setText("Algas e invertebrados del Parque");
                    lista.setAdapter(adaptadorAlgas);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
      //  spinAdaptador.notifyDataSetChanged();  No es necesario porque no se van a efectuar cambios sobre la vista del array

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent siguiente = new Intent(MainActivity.this, SegundoActivity.class);

                //Guardar valores para enviar al SegundoActivity y recogerlo en los Strings
                if (lista.getAdapter()==adaptadorPeces) {
                    siguiente.putExtra("datoNombre", al1.get(position).getNombre()+" ("+al1.get(position).getNombreLatin()+")");
                    siguiente.putExtra("datoImagen", al1.get(position).getImagen());
                } else {
                    siguiente.putExtra("datoNombre", al2.get(position).getNombre()+" ("+al2.get(position).getNombreLatin()+")");
                    siguiente.putExtra("datoImagen", al2.get(position).getImagen());
                }

                //Iniciar Intent (En este caso ir al SegundoActivity)
                startActivity(siguiente);
            }
        });
    }


    private void añadirElementosPeces() {
        try
        {
            String Linea;
            InputStream fraw =
                    getResources().openRawResource(R.raw.peces);

            BufferedReader brin =
                    new BufferedReader(new InputStreamReader(fraw));
            Linea = brin.readLine();
            while(Linea!=null) {
                //Sacar nombre imagen
                String [] pp = Linea.split(",");
                String uri = "@drawable/" + pp[0];
                int imagenEspecie = getResources().getIdentifier(uri, null, getPackageName());

                Linea = brin.readLine();

                // Pintar textView
                al1.add(new ListaPeces((pp[1] +"\n"+ pp[2] +"\n"+ pp[3] + " cm\n" + pp[4]), imagenEspecie, pp[1], pp[2]));
            };
            brin.close();

            al1.get(0).setImagen(R.drawable.babosa);
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al leer fichero desde recurso raw");
        }
    }

    private void añadirElementosAlgas() {
        try
        {
            String Linea;
            InputStream fraw =
                    getResources().openRawResource(R.raw.algaseinvertebrados);

            BufferedReader brin =
                    new BufferedReader(new InputStreamReader(fraw));
            Linea = brin.readLine();
            while(Linea!=null) {
                //Sacar nombre imagen
                String [] pp = Linea.split(",");
                String uri = "@drawable/" + pp[0];
                int imagenEspecie = getResources().getIdentifier(uri, null, getPackageName());

                Linea = brin.readLine();

                // Pintar textView
                if (pp[3]==null) {
                    al2.add(new ListaPeces((pp[1] +"\n"+ pp[4] + " cm\n" + pp[5]), imagenEspecie, pp[1], pp[4]));
                } else {
                    al2.add(new ListaPeces((pp[1] +"\n"+ pp[3] +"\n"+ pp[4] + " cm\n" + pp[5]), imagenEspecie, pp[1], pp[3]));
                }

            };
            brin.close();
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al leer fichero desde recurso raw");
        }
    }
}
