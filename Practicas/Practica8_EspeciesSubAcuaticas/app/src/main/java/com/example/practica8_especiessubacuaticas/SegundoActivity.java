package com.example.practica8_especiessubacuaticas;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SegundoActivity extends AppCompatActivity {

    private ScaleGestureDetector detector;
    private float xBegin=0;
    private float yBegin=0;

    TextView tituloImagen;
    ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segundo);

        //Dibujar flechita para volver
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tituloImagen = (TextView) findViewById(R.id.textView_NombreImagen);
        imagen = (ImageView) findViewById(R.id.imageView_ImagenAmpliada);

        //Dar valor a los Strings con la referencia enviada del intent 'siguiente'(MainActivity)
        String d_tituloImagen = getIntent().getStringExtra("datoNombre");
        int d_imagen = getIntent().getIntExtra("datoImagen", 0);

        //Poner el titulo en el actionBar
        getSupportActionBar().setTitle(d_tituloImagen);

        imagen.setImageResource(d_imagen);

        //Recoger la escala inicial de la imagen
        xBegin = imagen.getScaleX();
        yBegin = imagen.getScaleY();

        //Identificar cuando se contrae o expande la imagen
        detector = new ScaleGestureDetector(this, new ScaleListener(imagen, xBegin, yBegin));
    }

    //Para detectar cuando el usuario toque la pantalla usamos el evento
    public boolean onTouchEvent(MotionEvent evt){
        detector.onTouchEvent(evt);
        return super.onTouchEvent(evt);
    }

    //Lo que se hace al pulsar la flechita
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent catalogo = new Intent();
                setResult(RESULT_CANCELED, catalogo);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
