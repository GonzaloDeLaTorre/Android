﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileScript : MonoBehaviour
{
    private TileManager tilemanager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerExit(Collider other)
    {
        //probar que funciona mediante Debug.Log
        Debug.Log(gameObject);
        //llamada a la función del TileManager para generar una baldosa. Como este script está asociado
        //a un prefab a un no instanciado tendremos que usar la siguiente línea de código para encontrar
        //el gameobject Tilemanager
        tilemanager = GameObject.FindObjectOfType<TileManager>();
    }

}
