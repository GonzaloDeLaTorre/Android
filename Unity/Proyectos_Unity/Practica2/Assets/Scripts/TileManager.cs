﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    //public GameObject CurrentTile;
    public GameObject Tile;
    public GameObject NewPrefab;

    // Start is called before the first frame update
    void Start()
    {
        //Se crean diez baldosas aleatorias iniciales
        //llamando a CrearBaldosa()
       
        CrearBaldosa();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CrearBaldosa();
        }
    }

    public void CrearBaldosa()
    {
        for (int i = 0; i < Random.Range(3, 10); i++)
        {
            //Instantiate(Tile, transform.position, transform.rotation);
            Instantiate(NewPrefab, transform.position, transform.rotation);
            Invoke("CrearBaldosa", Random.Range(0, 3));
           
        }
        
        
        //este metodo es llamado desde Start
        //tambien desde el método TileScript
        //de los prefabs
        //genera un aleatorio 0,1
        //instancia un prefab y lo asocia a CurrentTile
        //mediante (GameObject)Instantiate
    }

}
